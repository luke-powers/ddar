Plan
====

Compare files in MESS with other files in photos, check if they are
unique, if unique rename and copy them to photo/.

If name collisions, add one to milliseconds.

sshing to machine, pxssh

pxssh replaces PS1 with its own prompt.

No need for pxssh, python2.7 on ds113j

Move to duplicates directory instead of remove

* need to be hard links since ds133j doesn't see symlinks in file browser


Emacs Processing
================

**Get rid of IMG_ and VID_**

::

   \(IMG_\|VID_\)\(.*\)
   \2

**Get rid of _HDR**

::

   \(.*\)_HDR\(.*\)
   \1\2

**Convert date**

::

   \([0-9]\{4\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\(.*\)
   \1-\2-\3T\4

**Convert time**

::

   \([^T]\)\([0-9]\{4\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\(.*\)
   \1T\2.\3

**Get rid of _**

::

   \(.*\)_\(.*\)
   \1\2

