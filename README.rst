========
 README
========

De-Duplicate and Rename

==============================
 What is this repository for?
==============================

* Useful for finding duplicates and renaming files on a synology diskstore.
* Version: not complete
* `Learn ReStructuredText <http://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_

======================
 How do I get set up?
======================

* git pull onto ds or copy to ds, run *ddar.py -h*
* Configuration

  * Todo, need to be able to init variables via a conf file.

* Dependencies

  * Python2.7

* How to run tests

  * *ddar.py -T*

=========================
 Contribution guidelines
=========================

Feel free to create a PR, but make sure to write tests at least as
basic as the existing tests.

===================
 Who do I talk to?
===================

Feel free to message me through bitbucket.
