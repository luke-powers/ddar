'''De-Duplicate And Rename
Remove duplicates and rename files to match the GOOD_FILENAME regex.

'''

import argparse
from datetime import datetime #not python3
import hashlib
import json
import os
import pipes
import re
import shlex
import shutil
from subprocess import Popen, PIPE
import sys

DRY_RUN = False
GOOD_FILENAME = r'\d{4}-\d{2}-\d{2}T\d{2}\.\d{2}\.\d{2}\.\d{3}'
GOOD_COMPLETE_FILENAME = r'\d{4}-\d{2}-\d{2}T\d{2}\.\d{2}\.\d{2}\.\d{3}\.[a-zA-Z]+'
# Look for a value in the file itself to use as filename.
IN_FILE_REGEX = r'\d{4}:\d{2}:\d{2} \d{2}:\d{2}:\d{2}' # Looking for 2013:10:24 18:36:06
HASH_FILE_FILE = 'hash_file_index'
FILE_HASH_FILE = 'file_hash_index'
FILE_SEARCH_BYTES = 5000
PATH = '/volume1/photo/'
FINISHED_DIRECTORY = '/volume1/photo/Display/'
WORK_DIRECTORY = '/volume1/homes/admin/.working/'

BLACKLIST_DIRECTORIES = ['Graphics', '@eaDir', '.sync', '.OutFocus', WORK_DIRECTORY]
BLACKLIST_FILES = ['.DS_Store']
WHITELIST_EXTENSIONS = ['.jpg', '.jpeg', '.avi', '.mp4', '.mpg', '.mov', '.png', '.tiff', '.tif', '.bmp']
for x in WHITELIST_EXTENSIONS[:]:
    WHITELIST_EXTENSIONS.append(x.upper())

#Regex to build YYYY-MM-DDTHH.MM.SS.sss.ext where ext is extension.
REGEX_PIPELINE = {1: {'method': 're',
                      'from': r'(IMG_|VID_)(.*)',
                      'to': r'\2'},
                  2: {'method': 'replace',
                      'from': '_HDR',
                      'to': ''},
                  3: {'method': 'replace',
                      'from': ':nopm:',
                      'to': ''},
                  4: {'method': 're',
                      'from': 'T',
                      'to': ''},
                  5: {'method': 'replace',
                      'from': ':',
                      'to': ''},
                  6: {'method': 'replace',
                      'from': ' ',
                      'to': ''},
                  7: {'method': 're', #Remove any strange character
                      'from': '([!@#$%^&*()_-]|(?:[.](?![\w]+$)))',
                      'to': ''},
                  8: {'method': 're', #convert date
                      'from': r'(\d{4})(\d{2})(\d{2})(.*)',
                      'to': r'\1-\2-\3T\4'},
                  9: {'method': 're', #convert time
                      'from': r'([^T]*)T(\d{2})(\d{2})(\d{2})(\d{0,3})?(\w*)',
                      'to': r'\1T\2.\3.\4.\5\6'},
                  10: {'method': 're', #No milliseconds
                      'from': r'(\d{4}-)(\d{2}-)(\d{2}T)(\d{2}\.)(\d{2}\.)(\d{2})\.(\.[a-zA-Z0-9]+)',
                      'to': r'\1\2\3\4\5\6.000\7'},
                  11: {'method': 're', #one milliseconds
                       'from': r'(\d{4}-)(\d{2}-)(\d{2}T)(\d{2}\.)(\d{2}\.)(\d{2})\.(\d{1})(\.[a-zA-Z0-9]+)',
                       'to': r'\1\2\3\4\5\6.00\7\8'},
                  12: {'method': 're', #two milliseconds
                       'from': r'(\d{4}-)(\d{2}-)(\d{2}T)(\d{2}\.)(\d{2}\.)(\d{2})\.(\d{2})(\.[a-zA-Z0-9]+)',
                       'to': r'\1\2\3\4\5\6.0\7\8'}}

HASH_FILE_INDEX = {}
FILE_HASH_INDEX = {}

class PhotoManipulationException(Exception):
    '''Exception to raise if error encountered.

    '''
    def __init__(self, msg=''):
        super(PhotoManipulationException, self).__init__(msg)


def generate_index_find_duplicates(directory):
    '''Generate index of photos and return the duplicates.

    '''
    contents = list_candidate_files(directory)
    files = [x for x in contents if os.path.isfile(os.path.join(directory, x)) \
             and not os.path.islink(os.path.join(directory, x))]
    duplicates = []
    count = 1
    sym = {0:'|', 1:'/', 2:'-', 3:'\\'}
    num = len(files)
    for name in files:
        sys.stdout.write('%s : %s of %s \r' % (sym[count%4], count, num))
        sys.stdout.flush()
        count += 1
        full_path = os.path.join(directory, name)
        if not name in BLACKLIST_FILES and full_path not in FILE_HASH_INDEX.keys():
            hashed = hash_the_given_file(full_path)
            if hashed in HASH_FILE_INDEX:
                HASH_FILE_INDEX[hashed].append(full_path)
                duplicates.append(tuple(HASH_FILE_INDEX[hashed]))
            else:
                HASH_FILE_INDEX[hashed] = [full_path]
            FILE_HASH_INDEX[full_path] = hashed
    sys.stdout.write('\n')
    sys.stdout.flush()
    return duplicates

def generate_rename_pair(filepath):
    '''Given the filepath, attempt to generate a rename target that is a
    GOOD_COMPLETE_FILENAME and return the filepath and the GOOD_COMPLETE_FILENAME
    rename. Returns False if unable to generate rename target.

    '''
    name = filepath.split('/')[-1]
    rename = name
    process = 1
    while not re.match(GOOD_COMPLETE_FILENAME, rename) and process < len(REGEX_PIPELINE)+1:
        #Want to check if rename is good after each stage of pipeline.
        if REGEX_PIPELINE[process]['method'] == 're':
            reg = re.compile(REGEX_PIPELINE[process]['from'])
            rename = reg.sub(REGEX_PIPELINE[process]['to'], rename)
        elif REGEX_PIPELINE[process]['method'] == 'replace':
            rename = rename.replace(REGEX_PIPELINE[process]['from'],
                                    REGEX_PIPELINE[process]['to'])
        process += 1
    if not re.match(GOOD_COMPLETE_FILENAME, rename):
        #Try stripping off anything except for a GOOD_FILENAME match and the extension
        if re.match(GOOD_FILENAME, rename):
            date = re.match(GOOD_FILENAME, rename).group()
            ext = rename.split('.').pop()
            rename = '.'.join([date,ext])
            if not re.match(GOOD_COMPLETE_FILENAME, rename):
                #Finally give up
                print('No more tries, cannot rename %s' % filepath)
                return False
        else:
            print('cannot rename %s' % filepath)
            return False
    rename_path = os.path.join(FINISHED_DIRECTORY, rename)
    if os.path.isfile(rename_path):
        rename_path = handle_name_conflict_clean(rename_path)
    return [filepath, rename_path]

def get_date_from_file(filepath):
    '''Attempt to get a date from the contents in the filepath, or from
    the mtime of the file. Returns None if not possible.

    '''
    sto, ste = run_cmd('head -c %s %s' % (FILE_SEARCH_BYTES, pipes.quote(filepath)))
    if ste:
        raise PhotoManipulationException('got error attempting to get date: %s' % ste)
    date = None
    match = re.search(IN_FILE_REGEX, sto)
    if match:
        date = match.group()
    else:
        date = datetime.fromtimestamp(os.stat(filepath).st_mtime).strftime('%Y:%m:%d %H:%M:%S')
    return date

def handle_name_conflict_clean(filepath):
    '''Given a filepath that matches GOOD_COMPLETE_FILENAME, increment the
    millisecond field until a new filepath that doesn't exist is
    found.

    '''
    corrected_filepath = filepath
    match = re.search(r'\d{4}-\d{2}-\d{2}T\d{2}\.\d{2}\.\d{2}\.(\d{3})\.[a-zA-Z]+', filepath)
    milliseconds = int(match.group(1))
    while os.path.isfile(corrected_filepath):
        match = re.search(r'\d{4}-\d{2}-\d{2}T\d{2}\.\d{2}\.\d{2}\.(\d{3})\.[a-zA-Z]+', corrected_filepath)
        if match:
            milliseconds += 1
            reg = re.compile(r'(\d{4}-)(\d{2}-)(\d{2}T)(\d{2}\.)(\d{2}\.)(\d{2})(\.\d{3}\.)([a-zA-Z]+)')
            corrected_filepath = reg.sub(r'\1\2\3\4\5\6.%03d.\8' % milliseconds, corrected_filepath)
    return corrected_filepath

def handle_name_conflict_dirty(filepath):
    '''Given a filepath, if it already exists, generate a new filepath
    that doesn't exist by appending _<counter>_ to the filename before
    the extension, or by appending _<counter>_ to the end if there is
    no extension.

    '''
    corrected_filepath = filepath
    counter = 1
    while os.path.isfile(corrected_filepath):
        ext = False
        if '.' in filepath:
            segments = filepath.split('.')
            ext = segments.pop()
            segments = segments + ['_%s_' % counter, ext]
            corrected_filepath = '.'.join(segments)
        else:
            corrected_filepath = filepath + '_%s_' % counter
        counter += 1
    return corrected_filepath

def hash_sha224(contents):
    '''Return the hexdigest of the given contents.

    '''
    return hashlib.sha224(contents).hexdigest()

def hash_the_given_file(filepath):
    '''Create a hash from the contents of the given filepath.

    '''
    sto, ste = run_cmd('head -c %s %s' % (FILE_SEARCH_BYTES, pipes.quote(filepath)))
    if ste:
        print(ste)
        raise PhotoManipulationException('unable to open filepath %s' % filepath)
    return hash_sha224(sto)

def initialize(args):
    '''Initialize all the globals with arguments from given argparse
    namespace.

    '''
    global DRY_RUN
    global GOOD_FILENAME
    global GOOD_COMPLETE_FILENAME
    global IN_FILE_REGEX
    global HASH_FILE_FILE
    global HASH_FILE_INDEX
    global FILE_HASH_FILE
    global FILE_HASH_INDEX
    global FILE_SEARCH_BYTES
    global PATH
    global FINISHED_DIRECTORY
    global WORK_DIRECTORY
    global BLACKLIST_DIRECTORIES
    global BLACKLIST_FILES
    global WHITELIST_EXTENSIONS
    global REGEX_PIPELINE
    for param in ['BLACKLIST_DIRECTORIES', 'BLACKLIST_FILES']:
        if getattr(args, param):
            for skip in getattr(args, param).split(','):
                globals()[param].append(skip.strip())

    if os.path.isfile(HASH_FILE_FILE):
        try:
            HASH_FILE_INDEX = json.load(open(HASH_FILE_FILE))
            FILE_HASH_INDEX = json.load(open(FILE_HASH_FILE))
        except ValueError:
            os.remove(HASH_FILE_FILE)
            try:
                os.remove(FILE_HASH_FILE)
            except OSError:
                pass
    DRY_RUN = args.dry_run
    WORK_DIRECTORY = args.working
    PATH = args.top_level
    FINISHED_DIRECTORY = args.display
    if not os.path.isdir(WORK_DIRECTORY):
        os.makedirs(WORK_DIRECTORY)
    for x in WHITELIST_EXTENSIONS[:]:
        WHITELIST_EXTENSIONS.append(x.upper())


def list_candidate_files(directory):
    '''List all the files in directory needing to be renamed to match
    GOOD_COMPLETE_FILENAME.

    '''
    global BLACKLIST_FILES
    global WHITELIST_EXTENSIONS
    ret_files = []

    contents = os.listdir(directory)
    for name in contents:
        for good_ext in WHITELIST_EXTENSIONS:
            if not name.endswith(good_ext):
                continue
            if not name in BLACKLIST_FILES:
                ret_files.append(os.path.join(directory, name))
    return ret_files

def list_subdirectories(directory):
    '''List the subdirectories for the given directory excluding
    directories in BLACKLIST_DIRECTORIES.

    '''
    return _list_subdirectories_os_walk(directory)

def _list_subdirectories_os_walk(directory):
    '''List subdirectories using os.walk. 400ms in test of 5177 subdirs.

    '''
    ret_dirs = []
    for dirpath, _, _ in os.walk(directory, followlinks=True):
        if dirpath == directory:
            continue
        cont = False
        for skip in BLACKLIST_DIRECTORIES:
            if skip in dirpath:
                cont = True
        if cont:
            continue
        if os.path.split(dirpath)[-1].startswith('.'):
            BLACKLIST_DIRECTORIES.append(dirpath)
            continue
        ret_dirs.append(dirpath)
    return ret_dirs

def _list_subdirectories_glob(directory, sub_dirs=None):
    '''List subdirectories using popen. 500ms in test dir of 5177 subdirs.

    '''
    from glob import glob
    sub_dirs = sub_dirs or []
    sub_dirs = [x.rstrip('/') for x in glob(directory+'/*/')]
    for dir_obj in sub_dirs:
        for new_dir in glob(dir_obj+'/*/'):
            sub_dirs.append(new_dir.rstrip('/'))
    return sub_dirs

def main():

    '''Main function to run when ddar.py is called.

    '''
    parser = argparse.ArgumentParser(description='''De-duplicator and renamer.
Run without arguments to generate indexes, then use -r or -d.''')
    parser.add_argument('-d', dest='duplicates', action='store_true', help='process duplicates')
    parser.add_argument('-D', dest='display', default=FINISHED_DIRECTORY,
                        help='where to place finished files')
    parser.add_argument('-n', dest='dry_run', action='store_true', help='Dry run, don\'t actually move.')
    parser.add_argument('-r', dest='rename', action='store_true', help='process renames')
    parser.add_argument('-t', dest='top_level', default=PATH, help='run ddar on this directory')
    parser.add_argument('-T', dest='test', action='store_true', help='run tests')
    parser.add_argument('-W', dest='working', default=WORK_DIRECTORY, help='working directory')
    parser.add_argument('--skip_directories', '--sd', dest='BLACKLIST_DIRECTORIES',
                        help='csv of extra directories to skip while processing.')
    parser.add_argument('--skip_files', '--sf', dest='BLACKLIST_FILES',
                        help='csv of extra files to skip while processing.')
    args = parser.parse_args()
    if args.test:
        global DRY_RUN
        DRY_RUN = False
        TEST()
    initialize(args)
    if args.duplicates:
        if HASH_FILE_INDEX:
            print('processing duplicates')
            process_duplicates()
        else:
            print('Indexes need to be generated, run ddar alone or with -t.')
        write_indexes()
        exit()
    if args.rename:
        if HASH_FILE_INDEX:
            print('processing renames')
            process_renames()
        else:
            print('Indexes need to be generated, run ddar alone or with -t.')
        write_indexes()
        exit()
    try:
        for idx_file in [HASH_FILE_FILE, FILE_HASH_FILE]:
            shutil.copyfile(idx_file,
                            'trash/%s.bkp-%s' % (idx_file,
                                                 datetime.now().strftime('%Y-%m-%dT%H-%M-%S')))
        print('generate subdirectories')
        directory_list = list_subdirectories(pipes.quote(args.top_level))
        print('generate indexes and find duplicates')
        for directory in directory_list:
            print('Working on %s' % directory)
            generate_index_find_duplicates(directory)
    except KeyboardInterrupt:
        write_indexes()
    write_indexes()

def process_duplicates():
    '''Using the HASH_FILE_INDEX move the duplicates to the duplicate
    direcetory and set up a symlink to the original.

    '''
    reg = re.compile(GOOD_COMPLETE_FILENAME)
    for hash_value in HASH_FILE_INDEX:
        if len(HASH_FILE_INDEX[hash_value]) > 1:
            for pick in HASH_FILE_INDEX[hash_value]:
                if reg.search(pick) and FINISHED_DIRECTORY in pick and  not '_source' in pick:
                    move_duplicates_of(pick, hash_value)
                    break
            else:
                for pick in HASH_FILE_INDEX[hash_value]:
                    #If it at least starts with what looks like a year
                    #try to use it, otherwise continue. If all else
                    #fails, still picks one.
                    if not os.path.split(pick)[-1].startswith('20'):
                        continue
                    if not '_source' in pick:
                        break
                move_duplicates_of(pick, hash_value)

def process_renames():
    '''Using the HASH_FILE_INDEX, attempt to rename non GOOD_COMPLETE_FILENAME
    files to GOOD_COMPLETE_FILENAME to the FINISHED_DIRECTORY.

    '''
    if DRY_RUN:
        renamer = lambda x, y: sys.stdout.write('rename:\n\t%s\t%s\n' % (x,y))
    else:
        renamer = os.rename
    for hash_value in HASH_FILE_INDEX:
        if len(HASH_FILE_INDEX[hash_value]) > 1:
            continue
        cont = False
        for skip in BLACKLIST_DIRECTORIES+BLACKLIST_FILES:
            if skip in HASH_FILE_INDEX[hash_value][0]:
                cont = True
        if cont:
            continue
        filepath = HASH_FILE_INDEX[hash_value][0]
        name = os.path.split(filepath)[-1]
        if not re.match(GOOD_COMPLETE_FILENAME, name):
            rename_pair = generate_rename_pair(filepath)
            if rename_pair:
                renamer(*rename_pair)
            else:
                print('try again')
                file_date = get_date_from_file(filepath)
                ext = '.%s' % filepath.split('.')[-1]
                if ext == filepath:
                    ext = ''
                if file_date:
                    try:
                        rename = os.path.join(os.path.split(filepath)[0], file_date+ext)
                    except:
                        print('ERROR RENAMING %s' % filepath)
                        raise
                    rename_pair = generate_rename_pair(rename)
                    if rename_pair:
                        renamer(filepath, rename_pair[-1])
                    else:
                        print('Cannot rename %s, giving up on %s.' % filepath, rename_pair[-1])

            HASH_FILE_INDEX[hash_value].remove(filepath)
            HASH_FILE_INDEX[hash_value].append(rename_pair[-1])

def move_duplicates_of(original, hash_value):
    '''Move the duplicates of original file with the given hash_value

    '''
    if DRY_RUN:
        renamer = lambda x, y: sys.stdout.write('rename dups:\n\t%s\t%s\n' % (x,y))
        linker = lambda x, y: sys.stdout.write('link:\n\t%s\t%s\n' % (x,y))
    else:
        linker = os.link
        renamer = os.rename
    duplicate_path = os.path.join(WORK_DIRECTORY, 'duplicates')
    if not os.path.isdir(duplicate_path):
        os.makedirs(duplicate_path)
    duplicates_raw = list(set(HASH_FILE_INDEX[hash_value])-set([original]))
    duplicates_scrubbed = []
    for dup in duplicates_raw:
        for skip in BLACKLIST_DIRECTORIES+BLACKLIST_FILES:
            if skip in dup:
                break
        else:
            duplicates_scrubbed.append(dup)
    for dup in duplicates_scrubbed[:]:
        name = os.path.split(dup)[-1]
        renamed_dup = os.path.join(duplicate_path, name)
        split_name = name.split('.')
        if split_name[0] == split_name[-1]:
            link = os.path.join(duplicate_path, split_name + '_source')
        else:
            link = os.path.join(duplicate_path, '.'.join(split_name[0:-1] + ['_source'] + [split_name[-1]]))
        if os.path.isfile(renamed_dup):
            renamed_dup = handle_name_conflict_dirty(renamed_dup)
        try:
            renamer(dup, renamed_dup)
        except OSError as exc:
            print('unable to rename %s to %s: %s' % (dup, renamed_dup, exc))
        HASH_FILE_INDEX[hash_value].remove(dup)
        del(FILE_HASH_INDEX[dup])
        FILE_HASH_INDEX[renamed_dup] = hash_value
        try:
            linker(original, link)
        except OSError as exc:
            print('link already exists between\n\t%s and %s' % (original, link))

def run_cmd(cmd):
    '''Run the given cmd with subprocess.Popen

    '''
    if DRY_RUN:
        print(cmd)
        return ('','')
    else:
        proc = Popen(shlex.split(cmd), stdout=PIPE, stderr=PIPE)
        return proc.communicate()

def write_indexes():
    '''Write the HASH_FILE_INDEX and FILE_HASH_INDEX to HASH_FILE_FILE and
    FILE_HASH_FILE.

    '''
    if not DRY_RUN:
        json.dump(HASH_FILE_INDEX, open(HASH_FILE_FILE, 'w'))
        json.dump(FILE_HASH_INDEX, open(FILE_HASH_FILE, 'w'))
    else:
        print('Dry run, not writing indexes')

def TEST():
    '''Simple tests of the above functions.

    '''
    global WORK_DIRECTORY
    global hash_sha224
    global FINISHED_DIRECTORY
    global WHITELIST_EXTENSIONS
    global HASH_FILE_INDEX
    test_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test')
    test_dup_dir = os.path.join(os.path.join(test_dir, 'dup_test'))

    def clear_test_environment():
        '''Move old test environment to trash

        '''
        #TODO this should be /tmp/trash with tmpreaper running or
        #TMPTIME set > 0.
        if os.path.isdir('test'):
            if not os.path.isdir('trash'):
                os.makedirs('trash')
                os.rename('test', 'trash/test')
            elif os.path.isdir('trash/test'):
                    count = 1
                    trash_path = 'trash/test_%s' % count
                    while os.path.isdir(trash_path):
                        count += 1
                        trash_path = 'trash/test_%s' % count
                    os.rename('test', trash_path)

    def generate_test_environment():
        if os.path.isdir(test_dir):
            run_cmd('rm -rf %s/test' % os.path.dirname(os.path.realpath(__file__)))
        for sub_dir in ['dup_test', 'wonky named dir (2)']:
            os.makedirs(os.path.join(test_dir, sub_dir))
        for dup_file in ['2015-16-17T18.19.20.222.tst', '2015-16-17T18.19.20.223.tst',
                         '2015-16-17T18.19.20.000.tst']:
            f = open(os.path.join(test_dup_dir, dup_file), 'w')
            f.write('0987654321abcdefghijklmnopqrstuvwxyz\n')
            f.close()
        open(os.path.join(test_dup_dir, '2015-16-17T18.19.20.224.tst'), 'a').close()
        f = open(os.path.join(test_dir, 'IMG_20151617181920333.tst'), 'w')
        f.write('0987654321\n')
        f.close()
        f = open(os.path.join(test_dir, 'date-inside.tst'), 'w')
        f.write('asdfasdfasdfasdf2015:16:17 18:19:20testetseafas')
        f.close()
        f = open(os.path.join(test_dir, 'no-date-inside.tst'), 'w').close()
        os.utime(os.path.join(test_dir, 'no-date-inside.tst'), (1430000000, 1430000000))
        open(os.path.join(test_dir, 'wonky named dir (2)', 'IMG_20151617181920333.tst'), 'a').close()
    clear_test_environment()
    generate_test_environment()
    ###
    ### Test
    ###
    res = list_subdirectories(test_dir)
    exp = ['dup_test', 'wonky named dir (2)']
    for result in res:
        for expected in exp:
            if expected in result:
                break
        else:
            print('list_subdirectories failed')
            print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = get_date_from_file(os.path.join(test_dir, 'date-inside.tst'))
    exp = '2015:16:17 18:19:20'
    if res != exp:
        print('get_date_from_file failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = get_date_from_file(os.path.join(test_dir, 'no-date-inside.tst'))
    exp = '2015:04:25 15:13:20'
    if res != exp:
        print('get_date_from_file no date inside failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = handle_name_conflict_clean(os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'))
    exp = os.path.join(test_dup_dir, '2015-16-17T18.19.20.225.tst')
    if res != exp:
        print('handle_name_conflict_clean failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = handle_name_conflict_clean(os.path.join(test_dup_dir, '2015-16-17T18.19.20.000.tst'))
    exp = os.path.join(test_dup_dir, '2015-16-17T18.19.20.001.tst')
    if res != exp:
        print('handle_name_conflict_clean failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = handle_name_conflict_dirty(os.path.join(test_dir, 'date-inside.tst'))
    exp = os.path.join(test_dir, 'date-inside._1_.tst')
    if res != exp:
        print('handle_name_conflict_dirty failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    WHITELIST_EXTENSIONS = ['.tst']
    res = list_candidate_files('/volume1/homes/admin/test')
    exp = [os.path.join(test_dir, 'IMG_20151617181920333.tst'),
           os.path.join(test_dir, 'date-inside.tst'),
           os.path.join(test_dir, 'no-date-inside.tst')]
    if set(res) != set(exp):
        print('list_candidate_files failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    FINISHED_DIRECTORY = test_dir
    res = generate_rename_pair(os.path.join(test_dir, 'IMG_20151617181920222.tst'))
    exp = [os.path.join(test_dir, 'IMG_20151617181920222.tst'),
           os.path.join(test_dir, '2015-16-17T18.19.20.222.tst')]
    if set(res) != set(exp):
        print('generate_rename_pair failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = generate_rename_pair(os.path.join(test_dir, 'IMG_20151617181920222_test_stuff.JPG'))
    exp = [os.path.join(test_dir, 'IMG_20151617181920222_test_stuff.JPG'),
           os.path.join(test_dir, '2015-16-17T18.19.20.222.JPG')]
    if set(res) != set(exp):
        print('generate_rename_pair CAP ext failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### Test
    ###
    res = generate_rename_pair(os.path.join(test_dir, '1057_11216_2014_test_test2.JPG'))
    exp = False
    if res != exp:
        print('generate_rename_pair unable to rename failed')
        print('res %s\nexp %s' % (res, exp))

    ###
    ### hash_the_given_file
    ###
    def test_hash_sha224(inp):
        '''Testing function for hash_the_given_file. Return None if the given
        inp doesn't match the known value. Return the expected 'out'
        otherwise.

        '''
        if inp != '0987654321\n':
            print('hash_the_given_file failed, not cating %s' % inp)
        return 'out'
    hash_sha224_bkp = hash_sha224
    hash_sha224 = test_hash_sha224
    res = hash_the_given_file(os.path.join(test_dir, 'IMG_20151617181920333.tst'))
    exp = 'out'
    if res != exp:
        print('hash_the_given_file failed')
        print('res %s\nexp %s' % (res, exp))
    hash_sha224 = hash_sha224_bkp

    ###
    ### Test
    ###
    res = generate_index_find_duplicates(test_dup_dir)
    exp = [(os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'),
            os.path.join(test_dup_dir, '2015-16-17T18.19.20.223.tst'))]
    exp_HASH_FILE_INDEX = {'d14a028c2a3a2bc9476102bb288234c415a2b01f828ea62ac5b3e42f':
                           [os.path.join(test_dup_dir, '2015-16-17T18.19.20.224.tst')],
                           '9dfc1c6fa155526d4c69131cb84554eb4aaea0d0eda7f9092714a157':
                           [os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'),
                            os.path.join(test_dup_dir, '2015-16-17T18.19.20.223.tst'),
                            os.path.join(test_dup_dir, '2015-16-17T18.19.20.000.tst')]}
    exp_FILE_HASH_INDEX = {os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'):
                           '9dfc1c6fa155526d4c69131cb84554eb4aaea0d0eda7f9092714a157',
                           os.path.join(test_dup_dir, '2015-16-17T18.19.20.223.tst'):
                           '9dfc1c6fa155526d4c69131cb84554eb4aaea0d0eda7f9092714a157',
                           os.path.join(test_dup_dir, '2015-16-17T18.19.20.224.tst'):
                           'd14a028c2a3a2bc9476102bb288234c415a2b01f828ea62ac5b3e42f',
                           os.path.join(test_dup_dir, '2015-16-17T18.19.20.000.tst'):
                           '9dfc1c6fa155526d4c69131cb84554eb4aaea0d0eda7f9092714a157'}
    if not res or set(res[0]) != set(exp[0]):
        print('generate_index_find_duplicates failed: wrong duplicates')
        if not res:
            print('generate_index_find_duplicates did not return anything')
        else:
            print('res %s\nexp %s' % (set(res[0]), set(exp[0])))
    if exp_HASH_FILE_INDEX != HASH_FILE_INDEX:
        print('generate_index_find_duplicates failed: bad HASH_FILE_INDEX')
        print('\nres \n%s\nexp \n%s' % (HASH_FILE_INDEX, exp_HASH_FILE_INDEX))
    if exp_FILE_HASH_INDEX != FILE_HASH_INDEX:
        print('generate_index_find_duplicates failed: bad FILE_HASH_INDEX')
        print('\nres \n%s\nexp \n%s' % (FILE_HASH_INDEX, exp_FILE_HASH_INDEX))

    ###
    ### Test
    ###
    HASH_FILE_INDEX = {'123abc': [os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'),
                                  os.path.join(test_dup_dir, '2015-16-17T18.19.20.223.tst')]}
    WORK_DIRECTORY = test_dir
    res = move_duplicates_of(os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'), '123abc')
    if not os.path.isdir(os.path.join(test_dir, 'duplicates')):
        print('move_duplicates_of failed: missing duplicates directory')
    if not os.path.isfile(os.path.join(test_dir, 'duplicates', '2015-16-17T18.19.20.223.tst')):
        print('move_duplicates_of failed: missing moved duplicate file')
    if not os.path.samestat(os.stat(os.path.join(test_dir,
                                                 'duplicates',
                                                 '2015-16-17T18.19.20.223._source.tst')),
                            os.stat(os.path.join(test_dup_dir, '2015-16-17T18.19.20.222.tst'))):
        print('move_duplicates_of failed: missing hardlink')

    ###
    ### Test
    ###
    clear_test_environment()
    generate_test_environment()
    move_duplicates_of

    exit(1)

if __name__ == '__main__':
    main()
